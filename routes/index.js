const express = require("express");
const router = express.Router();
const request = require("request");

/* GET home page. */
router.get("/", (req, res) => {
  //
  request(
    "http://phisix-api3.appspot.com/stocks.json",
    { json: true },
    (err, response, body) => {
      if (err) {
        return console.log(err);
      }
      res.json(body);
    }
  );
});

module.exports = router;
