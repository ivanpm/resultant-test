# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Test project for Resultant company
* Version 0.1

### How to run this project? ###

```
1. Open Terminal
2. Change directory to root folder of project - "cd .../resultant-test"
3. Install dependencies by command - "npm install"
4. Run application by command - "npm start"
5. Open next link in your Web Browser - http://127.0.0.1:8080
```
