import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import "./App.css";

class App extends Component {
  state = {
    stocks: true,
    data: []
  };

  getStocks() {
    fetch("/api", {
      accept: "application/json"
    })
      .then(response => {
        if (response.status === 200) return response.json();
        throw Error(response.statusText);
      })
      .then(responseJSON => {
        //console.log(responseJSON);
        this.setState({ data: responseJSON.stock });
      })
      .catch(err => {
        alert(err);
      });
  }

  componentDidMount() {
    this.getStocks();
    setInterval(() => {
      this.getStocks();
    }, 15000);
  }

  render() {
    const columns = [
      {
        Header: "Название",
        accessor: "name",
        Cell: props => <div style={{ height: "50px" }}>{props.value}</div>
      },
      {
        Header: "Цена",
        accessor: "volume",
        Cell: props => <span>{parseInt(props.value)}</span>
      },
      {
        Header: "Кол-во",
        accessor: "price.amount",
        Cell: props => <span>{parseFloat(props.value).toFixed(2)}</span>
      }
    ];

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Список валют</h1>
        </header>
        <div className="Content">
          {this.state.stocks !== undefined ? (
            <div className="Table">
              <ReactTable data={this.state.data} columns={columns} />
            </div>
          ) : (
            <div>Stocks not found</div>
          )}
          <button
            className="Button"
            onClick={() => {
              this.getStocks();
            }}
          >
            Обновить
          </button>
        </div>
      </div>
    );
  }
}

export default App;
